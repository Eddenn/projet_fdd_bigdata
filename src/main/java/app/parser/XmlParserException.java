package app.parser;

public class XmlParserException extends Exception {

    public XmlParserException(String message) {
        super(message);
    }
}
