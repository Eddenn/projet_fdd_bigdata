package app.parser.v2;

import app.model.Article;
import app.model.Mesh;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.*;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XmlParser {
    private static final String ELEMENT_PUBMEDARTICLE = "PubmedArticle";
    private static final String ELEMENT_MEDLINECITATION = "MedlineCitation";
    private static final String ELEMENT_PMID = "PMID";
    private static final String ELEMENT_ARTICLE = "Article";
    private static final String ELEMENT_MESHHEADINGLIST = "MeshHeadingList";
    private static final String ELEMENT_MESHHEADING = "MeshHeading";
    private static final String ELEMENT_DESCRIPTORNAME = "DescriptorName";
    private static final String ELEMENT_QUALIFIERNAME = "QualifierName";
    private static final String ELEMENT_ARTICLETITLE = "ArticleTitle";
    private static final String ELEMENT_ABSTRACT = "Abstract";
    private static final String ELEMENT_ABSTRACTTEXT = "AbstractText";

    private static final XMLInputFactory factory = XMLInputFactory.newInstance();
    private static FileWriter output = null;
    private static final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    private static boolean firstObject = true;

    public static void parse(String file) throws XMLStreamException, IOException {
        //Estimated time
        File inputFile = new File(file);
        int kbSize = (int)(inputFile.length()/1000);
        System.out.println("Size of the input : "+kbSize+"KB");

        InputStream inputStream = new FileInputStream(file);
        File outputFile = new File(file+".json");
        //File already exist and need to be empty
        if( !outputFile.createNewFile() ) {
            outputFile.delete();
            outputFile.createNewFile();
        }
        output = new FileWriter(outputFile,true);
        output.write('[');

        final XMLEventReader reader = factory.createXMLEventReader(inputStream);
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(ELEMENT_PUBMEDARTICLE)) {
                parsePubmedArticle(reader);
            }
        }

        output.write(']');
        output.close();
        System.out.println("Output JSON file :"+file+".json");
    }

    private static void parsePubmedArticle(final XMLEventReader reader) throws XMLStreamException {
        Article article = new Article();
        while (reader.hasNext()) {
            //<PubmedArticle>
            final XMLEvent event = reader.nextEvent();
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(ELEMENT_PUBMEDARTICLE)) {
                return;
            }
            if (event.isStartElement()) {
                final StartElement element = event.asStartElement();
                final String elementName = element.getName().getLocalPart();
                if(elementName.equals(ELEMENT_MEDLINECITATION)) {
                    //<MedlineCitation>
                    while (reader.hasNext()) {
                        final XMLEvent event1 = reader.nextEvent();
                        if (event1.isEndElement() && event1.asEndElement().getName().getLocalPart().equals(ELEMENT_MEDLINECITATION)) {
                            break;
                        }
                        if(event1.isStartElement()) {
                            final StartElement element1 = event1.asStartElement();
                            final String elementName1 = element1.getName().getLocalPart();
                            if(elementName1.equals(ELEMENT_PMID)) {
                                //<PMID>
                                article.setPmid(reader.getElementText());
                                //<PMID>
                            }
                            if(elementName1.equals(ELEMENT_ARTICLE)) {
                                //<Article>
                                while (reader.hasNext()) {
                                    final XMLEvent event2 = reader.nextEvent();
                                    if (event2.isEndElement() && event2.asEndElement().getName().getLocalPart().equals(ELEMENT_ARTICLE)) {
                                        break;
                                    }
                                    if (event2.isStartElement()) {
                                        final StartElement element2 = event2.asStartElement();
                                        final String elementName2 = element2.getName().getLocalPart();
                                        if(elementName2.equals(ELEMENT_ARTICLETITLE)) {
                                            //<ArticleTitle>
                                            while (reader.hasNext()) {
                                                final XMLEvent event6 = reader.nextEvent();
                                                if (event6.isEndElement() && event6.asEndElement().getName().getLocalPart().equals(ELEMENT_ARTICLETITLE)) {
                                                    break;
                                                }
                                                if (event6.isCharacters()) {
                                                    final Characters element6 = event6.asCharacters();
                                                    if(article.getAbstractText().equals("")) {
                                                        article.setTitleText(element6.getData());
                                                    } else {
                                                        article.appendTitleText(element6.getData());
                                                    }
                                                }
                                            }
                                            //</ArticleTitle>
                                        }
                                        if(elementName2.equals(ELEMENT_ABSTRACT)) {
                                            //<Abstract>
                                            while (reader.hasNext()) {
                                                final XMLEvent event3 = reader.nextEvent();
                                                if (event3.isEndElement() && event3.asEndElement().getName().getLocalPart().equals(ELEMENT_ABSTRACT)) {
                                                    break;
                                                }
                                                if (event3.isStartElement()) {
                                                    final StartElement element3 = event3.asStartElement();
                                                    final String elementName3 = element3.getName().getLocalPart();
                                                    if(elementName3.equals(ELEMENT_ABSTRACTTEXT)) {
                                                        //<AbstractText>
                                                        while (reader.hasNext()) {
                                                            final XMLEvent event4 = reader.nextEvent();
                                                            if (event4.isEndElement() && event4.asEndElement().getName().getLocalPart().equals(ELEMENT_ABSTRACTTEXT)) {
                                                                break;
                                                            }
                                                            if (event4.isCharacters()) {
                                                                final Characters element4 = event4.asCharacters();
                                                                if(article.getAbstractText().equals("")) {
                                                                    article.setAbstractText(element4.getData());
                                                                } else {
                                                                    article.appendAbstractText(element4.getData());
                                                                }
                                                            }
                                                        }
                                                        //</AbstractText>
                                                    }
                                                }
                                            }
                                            //</Abstract>
                                        }
                                    }
                                }
                                //</Article>
                            }
                            if(elementName1.equals(ELEMENT_MESHHEADINGLIST)) {
                                //<MeshHeadingList>
                                while (reader.hasNext()) {
                                    final XMLEvent event5 = reader.nextEvent();
                                    if (event5.isEndElement() && event5.asEndElement().getName().getLocalPart().equals(ELEMENT_MESHHEADINGLIST)) {
                                        break;
                                    }
                                    if (event5.isStartElement()) {
                                        final StartElement element5 = event5.asStartElement();
                                        final String elementName5 = element5.getName().getLocalPart();
                                        if(elementName5.equals(ELEMENT_MESHHEADING)) {
                                            //<MeshHeading>
                                            Mesh mesh = new Mesh();
                                            while (reader.hasNext()) {
                                                final XMLEvent event7 = reader.nextEvent();
                                                if (event7.isEndElement() && event7.asEndElement().getName().getLocalPart().equals(ELEMENT_MESHHEADING)) {
                                                    break;
                                                }
                                                if (event7.isStartElement()) {
                                                    final StartElement element7 = event7.asStartElement();
                                                    final String elementName7 = element7.getName().getLocalPart();
                                                    if(elementName7.equals(ELEMENT_DESCRIPTORNAME)) {
                                                        //<DescriptorName>
                                                        mesh.setDescription(reader.getElementText());
                                                        //</DescriptorName>
                                                    }
                                                    if(elementName7.equals(ELEMENT_QUALIFIERNAME)) {
                                                        //<QualifierName>
                                                        mesh.getQualifiers().add(reader.getElementText());
                                                        //</QualifierName>
                                                    }
                                                }
                                            }
                                            article.getMeshes().add(mesh);
                                            //</MeshHeading>
                                        }
                                    }
                                }
                                //</MeshHeadingList>
                            }
                        }
                    }
                    //</MedlineCitation>
                    break;
                }
            }
            //</PubmedArticle>
        }
        try {
            if(firstObject) {
                firstObject = false;
            } else {
                output.write(',');
            }
            output.write(ow.writeValueAsString(article));
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
