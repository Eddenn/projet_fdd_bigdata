package app.parser.v2_1;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;

public class XmlParser {
    private static final String ELEMENT_PUBMEDARTICLE = "PubmedArticle";
    private static final String ELEMENT_MEDLINECITATION = "MedlineCitation";
    private static final String ELEMENT_PMID = "PMID";
    private static final String ELEMENT_ARTICLE = "Article";
    private static final String ELEMENT_MESHHEADINGLIST = "MeshHeadingList";
    private static final String ELEMENT_MESHHEADING = "MeshHeading";
    private static final String ELEMENT_DESCRIPTORNAME = "DescriptorName";
    private static final String ELEMENT_QUALIFIERNAME = "QualifierName";
    private static final String ELEMENT_ARTICLETITLE = "ArticleTitle";
    private static final String ELEMENT_ABSTRACT = "Abstract";
    private static final String ELEMENT_ABSTRACTTEXT = "AbstractText";
    private static final String ELEMENT_PUBMEDDATA = "PubmedData";

    private static final XMLInputFactory factory = XMLInputFactory.newInstance();
    private static FileWriter output = null;
    private static final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    private static boolean firstObject = true;

    public static void parse(String file) throws XMLStreamException, IOException {
        //Estimated time
        File inputFile = new File(file);
        int kbSize = (int)(inputFile.length()/1000);
        System.out.println("Size of the input : "+kbSize+"KB");

        InputStream inputStream = new FileInputStream(file);
        File outputFile = new File(file+".json");
        //File already exist and need to be empty
        if( !outputFile.createNewFile() ) {
            outputFile.delete();
            outputFile.createNewFile();
        }
        output = new FileWriter(outputFile,true);
        output.write('[');

        final XMLEventReader reader = factory.createXMLEventReader(inputStream);
        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(ELEMENT_PUBMEDARTICLE)) {
                parseElement(reader,event);
            }
        }

        output.write(']');
        output.close();
        System.out.println("Output JSON file :"+file+".json");
    }

    private static void parseElement(XMLEventReader reader,XMLEvent firstEvent) throws IOException, XMLStreamException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstEvent.toString());

        while (reader.hasNext()) {
            final XMLEvent event = reader.nextEvent();
            if(event.isCharacters()) {
                String data = event.asCharacters().getData().trim();
                if(data.isEmpty() || data.equals(" ")) {
                    stringBuilder.append("<![CDATA[");
                    stringBuilder.append(event.asCharacters().getData());
                    stringBuilder.append("]]>");
                }
            } else {
                stringBuilder.append(event.toString());
            }
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(ELEMENT_PUBMEDARTICLE)) {
                break;
            }
        }

        System.out.println(stringBuilder.toString());
        JSONObject xmlJSONObj = XML.toJSONObject(stringBuilder.toString());
        String jsonPrettyPrintString = xmlJSONObj.toString(4);
        output.write(jsonPrettyPrintString);
        output.flush();
    }

}
