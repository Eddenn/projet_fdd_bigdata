package app.parser.v1;

import app.model.Article;
import app.model.Mesh;
import app.parser.XmlParserException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.net.URL;
import java.util.*;

public class XmlParser {

    /**
     * Factory used for parsing XML
     */
    private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    static {
        //Disable factory dtd loading and validation
        factory.setNamespaceAware(false);
        factory.setValidating(false);
        try {
            factory.setFeature("http://xml.org/sax/features/namespaces", false);
            factory.setFeature("http://xml.org/sax/features/validation", false);
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Parse an XML file and generate list of Articles
     * @param pathName path to XML file which contains Articles
     * @return list of Articles generated
     */
    public static List<Article> parse(String pathName) throws XmlParserException {
        List<Article> articleList = new ArrayList<Article>();

        //Read the file and generate a Document
        Document document;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            long startBuilderTime = System.currentTimeMillis();
            System.out.println("-> Start builder parsing");
            document = builder.parse(new File(pathName));
            System.out.println("-> Ended builder parsing in "+(System.currentTimeMillis()-startBuilderTime)+"ms");
        }
        catch (Exception e) {
            throw new XmlParserException("[ParserConfigurationException] "+e.getMessage());
        }

        //Use Document to generate Article
        Element root = document.getDocumentElement();
        if(!root.getTagName().equals("PubmedArticleSet")) {
            throw new XmlParserException("[XmlParser] "+"Root tag is not named PubmedArticleSet");
        }


        long startTime = System.currentTimeMillis();
        System.out.println("-> Starting Nodes parsing");
        for (Node art: asList(root.getElementsByTagName("PubmedArticle")) ) {
            long startArticleTime = System.nanoTime();
            Article article = new Article();
            for(Node artChild : asList(art.getChildNodes())) {
                //Search in MedlineCitation node
                if(artChild.getNodeName().equals("MedlineCitation")) {
                    for(Node medlineCitationChild : asList(artChild.getChildNodes())) {
                        //Set PMID
                        if(medlineCitationChild.getNodeName().equals("PMID")) {
                            article.setPmid( medlineCitationChild.getTextContent() );
                        //Search in Article node
                        } else if(medlineCitationChild.getNodeName().equals("Article")) {
                            for(Node articleNodeChild : asList(medlineCitationChild.getChildNodes())) {
                                //Set TitleText
                                if(articleNodeChild.getNodeName().equals("ArticleTitle")) {
                                    article.setTitleText( articleNodeChild.getTextContent() );
                                //Search in Abstract node
                                } else if(articleNodeChild.getNodeName().equals("Abstract")) {
                                    for(Node abstractChild : asList(articleNodeChild.getChildNodes())) {
                                        //Set AbstractText
                                        if(abstractChild.getNodeName().equals("AbstractText")) {
                                            if(article.getAbstractText().equals("")) {
                                                article.setAbstractText(abstractChild.getTextContent());
                                            } else {
                                                article.appendAbstractText(abstractChild.getTextContent());
                                            }
                                        }
                                    }
                                }
                            }
                        } else if((medlineCitationChild.getNodeName().equals("MeshHeadingList"))) {
                            //Search in MeshHeadingList node
                            for(Node meshHeadingListChild : asList(medlineCitationChild.getChildNodes())) {
                                if(meshHeadingListChild.getNodeName().equals("MeshHeading")) {
                                    Mesh mesh = new Mesh();
                                    //Search in MeshHeading node
                                    for(Node meshHeadingChild : asList(meshHeadingListChild.getChildNodes())) {
                                        //Set Description
                                        if(meshHeadingChild.getNodeName().equals("DescriptorName")) {
                                            mesh.setDescription(meshHeadingChild.getTextContent());
                                        //Set Qualifiers
                                        } else if(meshHeadingChild.getNodeName().equals("QualifierName")) {
                                            mesh.getQualifiers().add(meshHeadingChild.getTextContent());
                                        }
                                    }
                                    article.getMeshes().add(mesh);
                                }
                            }
                        }
                    }
                    break;
                }
            }
            articleList.add(article);
            System.out.println("--> Added one article in 0,00000"+(System.nanoTime()-startArticleTime)+"ms");
        }
        System.out.println("-> Ended parsing in "+(System.currentTimeMillis()-startTime)+"ms");

        return articleList;
    }

    /**
     * Utility method to apply foreach on NodeList
     * @param n the nodeList to convert
     * @return list
     */
    private static List<Node> asList(NodeList n) {
        return n.getLength()==0?
                Collections.<Node>emptyList(): new NodeListWrapper(n);
    }

    /**
     * Utility class to apply foreach on NodeList
     */
    static final class NodeListWrapper extends AbstractList<Node> implements RandomAccess {
        private final NodeList list;
        NodeListWrapper(NodeList l) {
            list=l;
        }
        public Node get(int index) {
            return list.item(index);
        }
        public int size() {
            return list.getLength();
        }
    }

}
