package app.model;

import java.util.ArrayList;
import java.util.List;

public class Mesh {
    private String description;

    private List<String> qualifiers;

    public Mesh() {
        this.description = "";
        this.qualifiers = new ArrayList<String>();
    }

    public Mesh(String description, List<String> qualifiers) {
        this.description = description;
        this.qualifiers = qualifiers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getQualifiers() {
        return qualifiers;
    }

    public void setQualifiers(List<String> qualifiers) {
        this.qualifiers = qualifiers;
    }

    @Override
    public String toString() {
        return "Mesh{" +
                "description='" + description + '\'' +
                ", qualifiers=" + qualifiers +
                '}';
    }
}
