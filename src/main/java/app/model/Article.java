package app.model;

import java.util.ArrayList;
import java.util.List;

public class Article {

    private String pmid;

    private String titleText;

    private String abstractText;

    private List<Mesh> meshes;

    public Article() {
        this.pmid = "";
        this.titleText = "";
        this.abstractText = "";
        this.meshes = new ArrayList<Mesh>();
    }

    public Article(String pmid, String titleText, String abstractText) {
        this.pmid = pmid;
        this.titleText = titleText;
        this.abstractText = abstractText;
        this.meshes = new ArrayList<Mesh>();
    }

    public Article(String pmid, String titleText, String abstractText, List<Mesh> meshes) {
        this.pmid = pmid;
        this.titleText = titleText;
        this.abstractText = abstractText;
        this.meshes = meshes;
    }

    public String getPmid() {
        return pmid;
    }

    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public void appendTitleText(String text) {
        this.titleText = " " + text;
    }

    public String getAbstractText() {
        return abstractText;
    }

    public void setAbstractText(String abstractText) {
        this.abstractText = abstractText;
    }

    public void appendAbstractText(String abstractText) {
        this.abstractText = " " + abstractText;
    }


    public List<Mesh> getMeshes() {
        return meshes;
    }

    public void setMeshes(List<Mesh> meshes) {
        this.meshes = meshes;
    }

    @Override
    public String toString() {
        return "Article{" +
                "pmid='" + pmid + "'\n" +
                ", titleText='" + titleText + "'\n" +
                ", abstractText='" + abstractText + "'\n" +
                ", meshes=" + meshes +
                '}';
    }
}
