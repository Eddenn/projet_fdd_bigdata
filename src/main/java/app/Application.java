package app;

import app.parser.v2.XmlParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Application {

    /**
     * Main application
     * @param args arguments
     */
    public static void main(String[] args) {
        String xmlfile;

        if(args.length == 1) {
            xmlfile = args[0];
        } else {
            System.out.println("USAGE: java -jar xmltojson.jar ./example.xml");
            return;
        }

        try {
            long startTime = System.currentTimeMillis();
            System.out.println("Start parsing ... (it may take time if input is big)");
            System.out.println("Example : 33seconds for an 1.5GB file with an i7 4700-HQ (8 core at 2.4GHz) on 64bits device.");
            XmlParser.parse(xmlfile);
            System.out.println("Parsing duration : "+(System.currentTimeMillis()-startTime)+"ms");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
